using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // gérer scènes, diff niveaux

public class BallScript : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody rb;
    private float xInput;
    private float yInput;   
    private int score = 0;

    public int winScore;
    public GameObject winText;

    private Vector3 startPosition;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }

    private void Update()
    {
        if (transform.position.y < -7f)
        {
            Respawn();
        }
    }

    void FixedUpdate()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");

        rb.AddForce(new Vector3(xInput, 0, yInput) * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Letter")
        {
            other.gameObject.SetActive(false);
            score++; // incrémentation chaque lettre collectée

            if (score >= winScore) // pour quand objectif atteint
            {
                WinLevel();
            }
        }
    }

    private void WinLevel()
    {
        winText.SetActive(true);
        StartCoroutine(GoToNextLevel());
    }

    private IEnumerator GoToNextLevel()
    {
        yield return new WaitForSeconds(4);
        GameManager.Instance.LoadNextLevel();
    }

    private void Respawn()
    {
        transform.position = startPosition;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
}
