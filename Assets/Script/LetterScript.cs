using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterController : MonoBehaviour
{
    public float rotationSpeed;
    private string letterID;

    private void Start()
    {
        // utilise nom objet comme ID unique
        letterID = gameObject.name;

        // si lettre collectée, objet désactivé
        if (GameManager.Instance.IsItemCollected(letterID))
        {
            gameObject.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        // rotation lettre
        transform.Rotate(0, rotationSpeed, 0);
    }

    // méthode pour collision
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // ajout ID de lettre au GameManager + désactivier l'objet
            GameManager.Instance.CollectItem(letterID);
            gameObject.SetActive(false);
        }
    }
}
