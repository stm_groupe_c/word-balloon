using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    private HashSet<string> collectedItems = new HashSet<string>();
    private int heartCount = 0;
    public GameObject hintImage; // Référence à l'image d'indice

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // pour être sûr que indice ne s'affiche pas 
        if (hintImage != null)
        {
            hintImage.SetActive(false);
        }
    }

    public void CollectItem(string itemId)
    {
        if (!collectedItems.Contains(itemId))
        {
            collectedItems.Add(itemId);
        }
    }

    public bool IsItemCollected(string itemId)
    {
        return collectedItems.Contains(itemId);
    }

    public void ResetCollectedItems()
    {
        collectedItems.Clear();
    }

    public void LoadNextLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        Debug.Log("Loading next level: " + nextSceneIndex); // Log pour vérifier si cette méthode est appelée
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.Log("Bravo, vous avez terminé tous les niveaux !");
            
        }
    }

    public void CollectHeart()
    {
        heartCount++;
    }

    private void Update()
    {
        // check si 'i' pressé & compteur coeur enough
        if (Input.GetKeyDown(KeyCode.I) && heartCount > 0)
        {
            heartCount--; // décrémente compteur
            StartCoroutine(ShowHint());
        }
    }

    private IEnumerator ShowHint()
    {
        hintImage.SetActive(true); // afficher indice
        yield return new WaitForSeconds(5); // délai 5 sec
        hintImage.SetActive(false); // cacher indice
    }
}
