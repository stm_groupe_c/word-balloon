using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{

    public float rotationSpeed;
    private string CoinID;

    // Start is called before the first frame update
    void Start()
    {
        CoinID = gameObject.name;

        if(GameManager.Instance.IsItemCollected(CoinID))
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        transform.Rotate(0,0,rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))      //chech si bien le joueur qui collide
        {
            GameManager.Instance.CollectItem(CoinID);
            gameObject.SetActive(false);
        }           
    }

}