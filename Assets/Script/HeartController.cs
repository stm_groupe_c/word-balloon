using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    public float speedRotation;

    private void Start()
    {
        // Ajoutez ici toute logique d'initialisation si nécessaire
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, 0, speedRotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // désactive coeur + incrémentation
            GameManager.Instance.CollectHeart();
            gameObject.SetActive(false);
        }
    }
}
