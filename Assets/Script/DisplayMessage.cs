using System.Collections;
using UnityEngine;

public class DisplayMessage : MonoBehaviour
{
    public GameObject messageImage; // réf image du msg
    private bool initialMessageShown = false;

    private void Start()
    {
        StartCoroutine(ShowInitialMessage());
    }

    private void Update()
    {
        // check si m pressé
        if (Input.GetKey(KeyCode.M))
        {
            messageImage.SetActive(true);
        }
        else if (initialMessageShown)
        {
            messageImage.SetActive(false);
        }
    }

    private IEnumerator ShowInitialMessage()
    {
        messageImage.SetActive(true); // print image
        yield return new WaitForSeconds(12); // délai début du jeu
        messageImage.SetActive(false); // hide image
        initialMessageShown = true; // msg initial montré
    }
}
